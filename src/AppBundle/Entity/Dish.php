<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * Dish
 *
 * @ORM\Table(name="dish")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DishRepository")
 * @Vich\Uploadable
 */
class Dish
{
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="dish_img_file", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;


    public function __construct()
    {
        $this->purchases = new ArrayCollection();
    }

    /**
     * @var $purchases Purchases
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Purchases",mappedBy="dish")
     */
    private $purchases;

    /**
     * @var $cafe Cafe
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cafe",inversedBy="dishes")
     */
    private $cafe;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Dish
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Dish
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Dish
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return Cafe
     */
    public function getCafe()
    {
        return $this->cafe;
    }

    /**
     * @param Cafe $cafe
     * @return Dish
     */
    public function setCafe($cafe)
    {
        $this->cafe = $cafe;
        return $this;
    }

    /**
     * @param Purchases $purchases
     * @return Dish
     */
    public function addPurchases($purchases)
    {
        $this->purchases->add($purchases);
        return $this;
    }

    /**
     * @param File $imageFile
     * @return Dish
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }
}

