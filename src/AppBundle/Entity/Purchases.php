<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Purchases
 *
 * @ORM\Table(name="purchases")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PurchasesRepository")
 */
class Purchases
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var Dish $dish
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Dish",inversedBy="purchases")
     */
    private $dish;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Purchases
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param Dish $dish
     * @return Purchases
     */
    public function setDish($dish)
    {
        $this->dish = $dish;
        return $this;
    }

    /**
     * @return Dish
     */
    public function getDish()
    {
        return $this->dish;
    }
}

