<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PagesControllerController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $cafes = $this->getDoctrine()->getRepository('AppBundle:Cafe')->findAll();

        $cafes = $this->getDoctrine()->getRepository('AppBundle:Dish')->findAllandTwoBest($cafes);

        return $this->render('@App/PagesController/index.html.twig', [
            'cafes' => $cafes
        ]);
    }

    /**
     * @Route("/view/{id}",requirements={"/d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction($id)
    {
        $cafe = $this->getDoctrine()->getRepository('AppBundle:Cafe')->find($id);
        return $this->render('@App/PagesController/view.html.twig', [
            'cafe' => $cafe
        ]);
    }

}
