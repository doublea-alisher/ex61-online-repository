<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Cafe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCafeData extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $cafes = ['Arzu', 'Versal'];
        $cafesImg = ['arzu.jpeg', 'versal.jpeg'];
        $cafesDesc = ['Новый ресторан после реконтсрукции', 'Ресторан для элиты из элит'];
        for ($i = 0; $i < 2; $i++) {
            $cafe = new Cafe();
            $cafe->setName($cafes[$i])
                ->setImage($cafesImg[$i])
                ->setDescription($cafesDesc[$i]);
            $manager->persist($cafe);

            $this->addReference($cafes[$i], $cafe);
        }
        $manager->flush();
    }
}