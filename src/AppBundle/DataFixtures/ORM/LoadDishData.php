<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Cafe;
use AppBundle\Entity\Dish;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDishData extends Fixture implements DependentFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $cafes = ['Arzu', 'Versal'];
        $dishes = ['Яичница', 'Плов', 'Говяжий язык', 'Мясо с картошкой', 'Буженина', 'Бефстроганов', 'Стейк', 'Гриль', 'Кебаб', 'Дымдама'];
        $dishesImg = ['yaika.jpeg', 'plov.jpeg', 'yazyk.jpeg', 'kartoshka-myaso.jpg', 'bugenina.jpg', 'befstroganov.jpg', 'steik.jpeg', 'grill.jpeg', 'kebab.jpeg', 'dymdama.jpeg'];

        for ($i = 0; $i < 2; $i++) {
            /** @var Cafe $cafe */
            $cafe = $this->getReference($cafes[$i]);
            for ($j = 0; $j < 5; $j++) {
                $dish = new Dish();
                $dish->setName(($i < 1) ? $dishes[$j] : $dishes[5 + $j])
                    ->setCafe($cafe)
                    ->setPrice(rand(200, 400))
                    ->setImage(($i < 1) ? $dishesImg[$j] : $dishesImg[5 + $j]);
                $manager->persist($dish);
                $this->addReference(($i < 1) ? $dishes[$j] : $dishes[5 + $j], $dish);
            }
        }
        $manager->flush();

    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadCafeData::class
        ];
    }
}