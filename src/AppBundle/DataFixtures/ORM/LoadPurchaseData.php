<?php
/**
 * Created by PhpStorm.
 * User: double
 * Date: 31.05.18
 * Time: 10:47
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Cafe;
use AppBundle\Entity\Purchases;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPurchaseData extends Fixture implements DependentFixtureInterface
{

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return [
            LoadCafeData::class,
            LoadDishData::class,
        ];
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $cafes = ['Arzu', 'Versal'];
        $dishes = ['Яичница', 'Плов', 'Говяжий язык', 'Мясо с картошкой', 'Буженина', 'Бефстроганов', 'Стейк', 'Гриль', 'Кебаб', 'Дымдама'];

        for ($i = 0; $i < 2; $i++) {
            /** @var Cafe $cafe */
            $cafe = $this->getReference($cafes[$i]);
            for ($j = 0; $j < 5; $j++) {
                $dish = $this->getReference(($i < 1) ? $dishes[$j] : $dishes[5 + $j]);
                $count = rand(6, 30);
                for ($p = 0; $p < $count; $p++) {
                    $purchase = new Purchases();
                    $date = new \DateTime();
                    $date->setDate(rand(2017, 2018), rand(1, 12), rand(1, 30));
                    $date->setTime(rand(9, 22), rand(0, 60), rand(0, 60));
                    $purchase->setDate($date);
                    $purchase->setDish($dish);
                    $manager->persist($purchase);
                }
            }
        }
        $manager->flush();
    }
}